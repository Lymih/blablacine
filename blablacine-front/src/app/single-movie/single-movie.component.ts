import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { CommentService } from '../comment.service';
import { Com, Movie, Review } from '../entities';

import { MovieService } from '../movie.service';
import { ReviewService } from '../review.service';
@Component({
  selector: 'app-single-movie',
  templateUrl: './single-movie.component.html',
  styleUrls: ['./single-movie.component.css']
})
export class SingleMovieComponent implements OnInit {
  movie?: Movie;
  routeId?: string;
  comments?: Com[];
  avg: number = 0;
  authState = this.auth.state;
  review: Review = { idUser: 1, idMovie: 1, score: 2 };
  constructor(private mS: MovieService, private route: ActivatedRoute, private router: Router, private coS: CommentService, private auth: AuthService, private rS: ReviewService) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => { this.routeId = param['id']; });
    this.mS.getById(Number(this.routeId)).subscribe(data => {
      this.movie = data;
      this.avg = this.mS.calcAVG(this.movie!);
    });
    this.coS.getByMovieId(Number(this.routeId)).subscribe(data => this.comments = data);

  }
  addItem(newItem: Com) {
    this.comments?.push(newItem);

  }

  rateMovie(review:Review){
    this.movie?.reviews?.push(review);
    this.avg = this.mS.calcAVG(this.movie!);
  }
  rateMovieOrNot() {
    return this.movie?.reviews?.find(review => review.idUser == this.authState.user?.id)
    
  }

  dec(num:number){
    return Math.round(num*100)/100;
  }
}
