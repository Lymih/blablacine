import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Movie } from './entities';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  
  constructor(private http:HttpClient) { }
  getAll(){
   return this.http.get<Movie[]>('movie/');
  }
  getById(id:number){
    return this.http.get<Movie>('movie/'+id);
  }
  searchByTitle(term:string){
    return this.http.get<Movie[]>('movie/search/'+term);
  }
  searchByCategory(id:number){
    return this.http.get<Movie[]>('movie/category/'+id);
  }
  add(movie:Movie){
    return this.http.post<Movie>('movie/',movie);
  }

  delete(id:number){
    return this.http.delete<number>('movie/'+id);
  }
  

  calcAVG(movie:Movie):number{
    let total:number=0;
    let avg:number;
    for(let item of movie.reviews!){
      total=item.score+total;
    }
    avg=total/movie.reviews!.length;
    return avg;
  }
}
