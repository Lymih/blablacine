import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from '../category.service';
import { Category, Movie } from '../entities';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-movie-form',
  templateUrl: './movie-form.component.html',
  styleUrls: ['./movie-form.component.css']
})
export class MovieFormComponent implements OnInit {
categories:Category[]=[];
@Input()
movie:Movie= {id:1,title:'',picture:'',year:'',director:'',description:'',category:{id:1,label:''}};

  constructor(private cS:CategoryService, private mS:MovieService, private router:Router ) { }

  onSubmit(){
    this.mS.add(this.movie).subscribe(()=>this.router.navigateByUrl('/'));
  }
  ngOnInit(): void {
    this.cS.getAll().subscribe(data=>this.categories=data);
  }

}
