import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuthService } from '../auth.service';
import { Movie, Review } from '../entities';
import { ReviewService } from '../review.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
  authState = this.auth.state;
  review: Review = { idUser: 1, idMovie: 1, score: 2 };

  @Input()
  movie?:Movie;

  @Output() ratedMovie = new EventEmitter<Review>();

  constructor(private rS:ReviewService, private auth:AuthService) { }

  ngOnInit(): void {

  }

  onSubmit() {
    this.review.idUser=this.authState.user!.id;
    this.review.idMovie=this.movie!.id;
    this.rS.add(this.review).subscribe((data)=>this.ratedMovie.emit(data));
    
  }
}
