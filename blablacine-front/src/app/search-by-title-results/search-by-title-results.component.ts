import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from '../entities';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-search-by-title-results',
  templateUrl: './search-by-title-results.component.html',
  styleUrls: ['./search-by-title-results.component.css']
})
export class SearchByTitleResultsComponent implements OnInit {
movies:Movie[]=[];
term?:string;
  constructor(private mS:MovieService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(param=>{this.term=param['term'];
    this.mS.searchByTitle(this.term!).subscribe(data=>this.movies=data);});
  }

}
