import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchByTitleResultsComponent } from './search-by-title-results.component';

describe('SearchByTitleResultsComponent', () => {
  let component: SearchByTitleResultsComponent;
  let fixture: ComponentFixture<SearchByTitleResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchByTitleResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchByTitleResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
