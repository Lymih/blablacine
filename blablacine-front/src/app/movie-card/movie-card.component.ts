import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Movie } from '../entities';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.css']
})
export class MovieCardComponent implements OnInit {

  @Input()
  movie:Movie={id:1,title:'',picture:'',year:'',director:'',description:'',category:{id:1,label:''}};
  authState=this.auth.state;
  avg:number=0;
  
  @Output()
  deletedMovie = new EventEmitter<Movie>();

  constructor(private auth:AuthService, private mS:MovieService, private router:Router) { }

 
  delete(){
    this.mS.delete(this.movie.id).subscribe(()=>this.deletedMovie.emit(this.movie));
  }
  ngOnInit(): void {
    this.avg=this.mS.calcAVG(this.movie);  
  }

  dec(num:number){
    return Math.round(num*100)/100;
  }
}
