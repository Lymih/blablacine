export interface Category {
    id:number;
    label:String;
}

export interface Movie {
    id:number;
    title:String;
    picture:String;
    year:String;
    director:String;
    description:String;
    category:Category;
    reviews?:Review[];
}
export interface User {
    id:number;
    email:string;
    password:string;
    role:string;
}

export interface Com{
    id?:number;
    content:string;
    date?:Date;
    movie?:Movie;
    user?:User;
}
export interface Review{
    idUser:number;
    idMovie:number;
    score:number;
}

