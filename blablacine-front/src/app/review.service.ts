import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Review } from './entities';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  constructor(private http:HttpClient) { }

  add(review:Review){
    return this.http.post<Review>('review/',review);
  }

}
