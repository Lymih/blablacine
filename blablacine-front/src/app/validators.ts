import { AbstractControl, AsyncValidatorFn, ValidationErrors, ValidatorFn } from "@angular/forms";
import { catchError, map, of } from "rxjs";
import { AuthService } from "./auth.service";


export function hasUpperValidator(): ValidatorFn {
    return (control:AbstractControl): ValidationErrors | null => {
        const value = control.value;
        if (!value) {
            return null;
        }
        const hasUpperCase = /[A-Z]+/.test(value);
        return !hasUpperCase 
            ? {hasUpperValidator:true}
            : null;
    }
}

export function matchPassword(control: AbstractControl): ValidationErrors | null {
 
    const password = control!.get("password")?.value;
    const confirm = control!.get("repeatPassword")?.value;
 
 
    if (password != confirm) { return { 'noMatch': true } }
 
    return null
 
  }

export function emailTaken(authService:AuthService): AsyncValidatorFn {
    return (control:AbstractControl) => 
    authService.userExists(control.value).pipe(
        map(() => null),
        catchError(() => of({emailTaken: true}))
    );
}
