import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from '../entities';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-edit-movie',
  templateUrl: './edit-movie.component.html',
  styleUrls: ['./edit-movie.component.css']
})
export class EditMovieComponent implements OnInit {
  movie:Movie = {id:1,title:'',picture:'',year:'',director:'',description:'',category:{id:1,label:''}};
  routeId?:string;
  constructor(private mS:MovieService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(param=>{this.routeId=param['id']});
    this.mS.getById(Number(this.routeId)).subscribe(data=>this.movie=data);
  }

}
