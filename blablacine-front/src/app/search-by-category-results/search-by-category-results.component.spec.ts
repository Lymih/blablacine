import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchByCategoryResultsComponent } from './search-by-category-results.component';

describe('SearchByCategoryResultsComponent', () => {
  let component: SearchByCategoryResultsComponent;
  let fixture: ComponentFixture<SearchByCategoryResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchByCategoryResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchByCategoryResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
