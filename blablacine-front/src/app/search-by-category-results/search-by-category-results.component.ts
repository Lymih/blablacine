import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from '../entities';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-search-by-category-results',
  templateUrl: './search-by-category-results.component.html',
  styleUrls: ['./search-by-category-results.component.css']
})
export class SearchByCategoryResultsComponent implements OnInit {
movies:Movie[]=[];
routeId?:string;
  constructor(private mS:MovieService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(param => {this.routeId=param['id'];});
    this.mS.searchByCategory(Number(this.routeId)).subscribe(data=>this.movies=data);
    console.log(this.movies);
  }

}
