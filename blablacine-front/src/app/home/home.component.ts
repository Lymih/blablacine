import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Movie } from '../entities';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
movies:Movie[]=[];

authState=this.auth.state;
  constructor(private mS:MovieService, private auth:AuthService) { }

  ngOnInit(): void {
  this.mS.getAll().subscribe(data =>this.movies=data)
  }
  deleteMovie(movie:Movie){
    this.movies.splice(this.movies.findIndex(item=>movie.id==item.id),1)
  }
}
