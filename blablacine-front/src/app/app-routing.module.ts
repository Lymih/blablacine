import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { EditMovieComponent } from './edit-movie/edit-movie.component';
import { HomeComponent } from './home/home.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { SearchByCategoryResultsComponent } from './search-by-category-results/search-by-category-results.component';
import { SearchByTitleResultsComponent } from './search-by-title-results/search-by-title-results.component';
import { SingleMovieComponent } from './single-movie/single-movie.component';

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'movie/:id',component:SingleMovieComponent},
  {path:'search/:term',component:SearchByTitleResultsComponent},
  {path:'category/:id',component:SearchByCategoryResultsComponent},
  {path:'login',component:LoginPageComponent},
  {path:'register',component:RegisterPageComponent},
  {path:'add-movie',component:AddMovieComponent},
  {path:'edit-movie/:id',component:EditMovieComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
