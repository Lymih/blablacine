import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { hasUpperValidator, matchPassword } from '../validators';



@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {
  
  form = this.fb.group({
    email: ['', [Validators.email, Validators.required]],
    password: [
      '',
      [
        Validators.minLength(4),
        Validators.required,
        
      ]
    ],
    repeatPassword: ['', Validators.required]
  }, { validators: matchPassword });

  hasError = false;
  emailTaken = false;

  constructor(private auth: AuthService, private router: Router, public fb: FormBuilder) { }


  ngOnInit(): void {
  }

  get email() {
    return this.form.get('email');
  }
  get password() {
    return this.form.get('password');
  }

  register() {
    this.hasError = false;
    if (this.form.valid) {
      this.auth.register(this.form.value).subscribe({
        // Tout s'est bien passé, on fait le next
        next: data => this.router.navigate(['/']),
        // Il y a eu un problème, on rentre dans le error
        error: () => this.hasError = true
      });
    }

  }

  verifyEmail() {
    this.emailTaken = false;
    if (this.email?.valid) {
      this.auth.userExists(this.email?.value).subscribe({
        error:()=>this.emailTaken=true
      });
    }
  }
  checkPasswords: ValidatorFn = (group: AbstractControl):  ValidationErrors | null => { 
    let pass = group.get('password')?.value;
    let confirmPass = group.get('confirmPassword')?.value
    return pass === confirmPass ? null : { notSame: true }
  }
}

