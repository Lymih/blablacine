import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { HomeComponent } from './home/home.component';
import { MovieCardComponent } from './movie-card/movie-card.component';
import { SingleMovieComponent } from './single-movie/single-movie.component';
import { SearchByTitleResultsComponent } from './search-by-title-results/search-by-title-results.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchByCategoryResultsComponent } from './search-by-category-results/search-by-category-results.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { CredentialInterceptor } from './credential.interceptor';
import { CommentCardComponent } from './comment-card/comment-card.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { CommentFormComponent } from './comment-form/comment-form.component';
import { MovieFormComponent } from './movie-form/movie-form.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { EditMovieComponent } from './edit-movie/edit-movie.component';
import { ReviewComponent } from './review/review.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchBarComponent,
    HomeComponent,
    MovieCardComponent,
    SingleMovieComponent,
    SearchByTitleResultsComponent,
    SearchByCategoryResultsComponent,
    LoginPageComponent,
    CommentCardComponent,
    RegisterPageComponent,
    CommentFormComponent,
    MovieFormComponent,
    AddMovieComponent,
    EditMovieComponent,
    ReviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    {provide:HTTP_INTERCEPTORS,useClass:CredentialInterceptor,multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
