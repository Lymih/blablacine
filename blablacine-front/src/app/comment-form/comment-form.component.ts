import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuthService } from '../auth.service';
import { CommentService } from '../comment.service';
import { Com, Movie } from '../entities';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit {
  @Input()
  movie?:Movie;
  @Output() newItemEvent = new EventEmitter<Com>();
  comment:Com={content:""};
  authState=this.auth.state;
  constructor(private auth:AuthService, private comS:CommentService) { }
  post(){
    if(this.authState.user){
    this.comment.date= new Date();
    this.comment.user=this.authState.user;
    this.comment.movie=this.movie;
    this.comS.add(this.comment).subscribe();
    console.log(this.comment);
    this.newItemEvent.emit(this.comment);
    }
  }
  ngOnInit(): void {

  }

}
