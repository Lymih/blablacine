import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  term?:string;
  constructor(private router:Router) { }

  onSubmit(){
    this.router.navigateByUrl('search/'+this.term)
  }
  ngOnInit(): void {
  }

}
