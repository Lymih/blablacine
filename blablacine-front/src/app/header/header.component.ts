import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  authState=this.auth.state;
  constructor(private auth:AuthService) { }

  logout(){
    this.auth.logout().subscribe();
  }
  ngOnInit(): void {
  }

}
