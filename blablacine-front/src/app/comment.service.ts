import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Com } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http:HttpClient) { }

  getByMovieId(id:number){
  return this.http.get<Com[]>('comment/movie/'+id)
  }
  add(comment:Com){
    return this.http.post<Com>('comment',comment)
  }
}
