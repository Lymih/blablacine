import { Component, Input, OnInit } from '@angular/core';
import { Com } from '../entities';

@Component({
  selector: 'app-comment-card',
  templateUrl: './comment-card.component.html',
  styleUrls: ['./comment-card.component.css']
})
export class CommentCardComponent implements OnInit {

@Input()
comment?:Com;
  constructor() { }

  ngOnInit(): void {
  }

}
