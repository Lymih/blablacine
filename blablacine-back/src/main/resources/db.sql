DROP DATABASE IF EXISTS blablacine;
CREATE DATABASE IF NOT EXISTS blablacine;

USE blablacine;


CREATE TABLE category (
  id TINYINT UNSIGNED NOT NULL  AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR(64) NOT NULL
   );


CREATE TABLE movie (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  title VARCHAR(120) NOT NULL,
  picture VARCHAR(120) NOT NULL,
  year CHAR(4) NOT NULL,
  director VARCHAR(120) NOT NULL,
  description TEXT NOT NULL,
  id_category TINYINT UNSIGNED NOT NULL,
  CONSTRAINT FK_movie_category FOREIGN KEY (id_category) REFERENCES category(id) ON DELETE CASCADE  ON UPDATE CASCADE
);



CREATE TABLE user (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(64) NOT NULL,
  role VARCHAR(64) NOT NULL,
  password VARCHAR(128) NOT NULL
);

CREATE TABLE review (
  id_user INT UNSIGNED NOT NULL,
  id_movie INT UNSIGNED NOT NULL,
  score FLOAT UNSIGNED,
  PRIMARY KEY (id_user, id_movie),
  CONSTRAINT FK_review_user FOREIGN KEY (id_user) REFERENCES user(id)  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_review_movie FOREIGN KEY (id_movie) REFERENCES movie(id) ON DELETE CASCADE ON UPDATE CASCADE

);

CREATE TABLE comment (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    content VARCHAR(500) NOT NULL,
    date DATE NOT NULL,
    id_user INT UNSIGNED NOT NULL,
    id_movie INT UNSIGNED NOT NULL,
    CONSTRAINT FK_comments_user FOREIGN KEY(id_user) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FK_comments_movie FOREIGN KEY(id_movie) REFERENCES movie(id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO
    category (label)
VALUES
    ('Epouvante-horreur'),
    ('Policier'),
    ('Fantastique'),
    ('Aventure'),
    ('Animation'),
    ('Documentaire'),
    ('Biopic'),
    ('Guerre'),
    ('Drame'),
    ('Comédie'),
    ('Romance'),
    ('Science fiction');

INSERT INTO
    movie (
        title,
        picture,
        year,
        director,
        description,
        id_category
    )
VALUES
    (
        'The deep house',
        'https://fr.web.img2.acsta.net/c_310_420/pictures/21/05/21/13/39/2276589.jpg',
        '2021',
        'Alexandre Bustillo, Julien Maury',
        'Un jeune couple américain spécialisé dans l\'urbex (exploration urbaine) décide d’aller explorer une maison réputée hantée qui a été ensevelie sous un lac artificiel. Mais celle-ci semble se refermer sur eux et le couple se retrouve prisonnier de cet endroit chargé des plus sombres histoires…',
        1
    ),
    (
        'Infiltré',
        'https://fr.web.img2.acsta.net/c_310_420/medias/nmedia/18/96/11/41/20446696.jpg',
        '2013',
        'Ric Roman Waugh',
        'John Matthews, un homme d’affaires, est dévasté lorsque son fils Jason, 18 ans, est condamné à dix ans de prison : il a été arrêté en possession d’un paquet de drogue envoyé par un de ses amis, mais il ignorait tout de son contenu. John propose alors un marché au procureur : il va infiltrer le plus redoutable des cartels de la drogue afin d’en faire tomber les têtes en échange d’une réduction de peine. Au cœur de l’organisation, il va mettre la vie de beaucoup de monde en jeu, à commencer par la sienne…',
        2
    ),
    (
        'Le Monde de Narnia : Le Lion, la Sorcière blanche et l\'Armoire magique',
        'https://fr.web.img6.acsta.net/c_310_420/medias/nmedia/18/35/53/32/18463695.jpg',
        '2005',
        'Andrew Adamson',
        'Le Monde de Narnia : chapitre 1 conte la lutte entre le bien et le mal qui oppose le magnifique lion Aslan aux forces des ténèbres dans le monde magique de Narnia. Grâce à ses sombres pouvoirs, la Sorcière Blanche a plongé Narnia dans un hiver qui dure depuis un siècle, mais une prédiction révèle que quatre enfants aideront Aslan à rompre la malédiction. Lorsque Lucy, Susan, Edmund et Peter Pevensie, quatre frères et soeurs, découvrent ce monde enchanté en y pénétrant à travers une armoire, tout est en place pour une bataille de proportions épiques...',
        3
    ),
    (
        'Les aventuriers de l\'arche perdue',
        'https://fr.web.img2.acsta.net/c_310_420/medias/nmedia/00/02/49/18/affiche.jpg',
        '1981',
        'Steven Spielberg',
        '1936. Parti à la recherche d\'une idole sacrée en pleine jungle péruvienne, l\'aventurier Indiana Jones échappe de justesse à une embuscade tendue par son plus coriace adversaire : le Français René Belloq.
Revenu à la vie civile à son poste de professeur universitaire d\'archéologie, il est mandaté par les services secrets et par son ami Marcus Brody, conservateur du National Museum de Washington, pour mettre la main sur le Médaillon de Râ, en possession de son ancienne amante Marion Ravenwood, désormais tenancière d\'un bar au Tibet.
Cet artefact égyptien serait en effet un premier pas sur le chemin de l\'Arche d\'Alliance, celle-là même où Moïse conserva les Dix Commandements. Une pièce historique aux pouvoirs inimaginables dont Hitler cherche à s\'emparer...',
        4
    ),
    (
        'Wall-E',
        'https://fr.web.img6.acsta.net/c_310_420/medias/nmedia/18/63/93/01/18948378.jpg',
        '2008',
        'Andrew Stanton',
        'Faites la connaissance de WALL•E (prononcez "Walli") : WALL•E est le dernier être sur Terre et s\'avère être un... petit robot ! 700 ans plus tôt, l\'humanité a déserté notre planète laissant à cette incroyable petite machine le soin de nettoyer la Terre. Mais au bout de ces longues années, WALL•E a développé un petit défaut technique : une forte personnalité. Extrêmement curieux, très indiscret, il est surtout un peu trop seul...
Cependant, sa vie s\'apprête à être bouleversée avec l\'arrivée d\'une petite "robote", bien carénée et prénommée EVE. Tombant instantanément et éperdument amoureux d\'elle, WALL•E va tout mettre en oeuvre pour la séduire. Et lorsqu\'EVE est rappelée dans l\'espace pour y terminer sa mission, WALL•E n\'hésite pas un seul instant : il se lance à sa poursuite... Hors de question pour lui de laisser passer le seul amour de sa vie... Pour être à ses côtés, il est prêt à aller au bout de l\'univers et vivre la plus fantastique des aventures !',
        5
    ),
    (
        'Microcosmos: Le peuple de l\'herbe',
        'https://fr.web.img3.acsta.net/c_310_420/medias/nmedia/18/70/18/27/19090499.jpg',
        '1996',
        'Claude Nuridsany, Marie Pérennou',
        'Voyage sur terre à l\'echelle du centimètre. Ses habitants: insectes et autres animaux de l\'herbe et de l\'eau. Grand prix de la commission supérieure technique, Festival de Cannes 1996.',
        6
    ),
    (
        'Les heures sombres',
        'https://fr.web.img2.acsta.net/c_310_420/pictures/17/10/19/12/32/5360469.jpg',
        '2018',
        'Joe Wright',
        'Homme politique brillant et plein d’esprit, Winston Churchill est un des piliers du Parlement du Royaume-Uni, mais à 65 ans déjà, il est un candidat improbable au poste de Premier Ministre. Il y est cependant nommé d’urgence le 10 mai 1940, après la démission de Neville Chamberlain, et dans un contexte européen dramatique marqué par les défaites successives des Alliés face aux troupes nazies et par l’armée britannique dans l’incapacité d’être évacuée de Dunkerque.
Alors que plane la menace d’une invasion du Royaume- Uni par Hitler et que 200 000 soldats britanniques sont piégés à Dunkerque, Churchill découvre que son propre parti complote contre lui et que même son roi, George VI, se montre fort sceptique quant à son aptitude à assurer la lourde tâche qui lui incombe. Churchill doit prendre une décision fatidique : négocier un traité de paix avec l’Allemagne nazie et épargner à ce terrible prix le peuple britannique ou mobiliser le pays et se battre envers et contre tout.
Avec le soutien de Clémentine, celle qu’il a épousée 31 ans auparavant, il se tourne vers le peuple britannique pour trouver la force de tenir et de se battre pour défendre les idéaux de son pays, sa liberté et son indépendance. Avec le pouvoir des mots comme ultime recours, et avec l’aide de son infatigable secrétaire, Winston Churchill doit composer et prononcer les discours qui rallieront son pays. Traversant, comme l’Europe entière, ses heures les plus sombres, il est en marche pour changer à jamais le cours de l’Histoire.',
        7
    ),
    (
        'Il faut sauver le soldat Ryan',
        'https://fr.web.img6.acsta.net/c_310_420/medias/04/22/53/042253_af.jpg',
        '1998',
        'Steven Spielberg',
        'Alors que les forces alliées débarquent à Omaha Beach, Miller doit conduire son escouade derrière les lignes ennemies pour une mission particulièrement dangereuse : trouver et ramener sain et sauf le simple soldat James Ryan, dont les trois frères sont morts au combat en l\' espace de trois jours.Pendant que l \'escouade progresse en territoire ennemi, les hommes de Miller se posent des questions. Faut-il risquer la vie de huit hommes pour en sauver un seul ?',
        8
    ),
    ('Forest Gump',
    'https://fr.web.img2.acsta.net/c_310_420/pictures/15/10/13/15/12/514297.jpg',
    '1994',
    'Robert Zemeckis',
    'Quelques décennies d\'histoire américaine, des années 1940 à la fin du XXème siècle, à travers le regard et l\'étrange odyssée d\'un homme simple et pur, Forrest Gump.',
    9
    ),
    ('Dumb and Dumber',
    'https://fr.web.img6.acsta.net/c_310_420/medias/nmedia/18/70/93/55/19754569.jpg',
    '1994',
    'Peter Farrelly, Bobby Farrelly',
    'Après un malentendu à l\'aéroport, deux amis, Lloyd et Harry, spécialistes de l\'élevages de lombrics, partent à la recherche de Mary qui semble y avoir oubliée sa valise. Mais, ils se retrouvent au centre d\'un complot.',
    10),
    ('Call me by your name',
    'https://fr.web.img4.acsta.net/c_310_420/o_club300-310x420.png_0_se/pictures/18/02/13/15/35/4076138.jpg',
    '2017',
    'Luca Guadagnino',
    'Été 1983. Elio Perlman, 17 ans, passe ses vacances dans la villa du XVIIe siècle que possède sa famille en Italie, à jouer de la musique classique, à lire et à flirter avec son amie Marzia. Son père, éminent professeur spécialiste de la culture gréco-romaine, et sa mère, traductrice, lui ont donné une excellente éducation, et il est proche de ses parents. Sa sophistication et ses talents intellectuels font d’Elio un jeune homme mûr pour son âge, mais il conserve aussi une certaine innocence, en particulier pour ce qui touche à l’amour. Un jour, Oliver, un séduisant Américain qui prépare son doctorat, vient travailler auprès du père d’Elio. Elio et Oliver vont bientôt découvrir l’éveil du désir, au cours d’un été ensoleillé dans la campagne italienne qui changera leur vie à jamais.',
    11),
    ('Interstellar',
    'https://fr.web.img5.acsta.net/c_310_420/pictures/14/09/24/12/08/158828.jpg',
    '2014',
    'Christopher Nolan, Jonathan Nolan',
    'Le film raconte les aventures d’un groupe d’explorateurs qui utilisent une faille récemment découverte dans l’espace-temps afin de repousser les limites humaines et partir à la conquête des distances astronomiques dans un voyage interstellaire.',
    12),
    ('The grudge',
    'https://fr.web.img5.acsta.net/c_310_420/pictures/19/12/12/12/07/5438128.jpg',
    '2020',
    'Nicolas Pesce',
    'Une nouvelle version tortueuse de ce classique du genre, de cette histoire horrifique, inspirée du film JU-ON : THE GRUDGE de Takashi Shimizu.',
    1)
    ;

INSERT INTO user (email,role,password) VALUES  ("admin@admin.com","ROLE_ADMIN","$2a$12$SGLvnoEzm0jB/YLb/TCYx.8IcyZMQLWzuFdHRddAAku1hVkJH5LHu");
INSERT INTO user (email,role,password) VALUES ("user@user.com","ROLE_USER","$2a$12$SGLvnoEzm0jB/YLb/TCYx.8IcyZMQLWzuFdHRddAAku1hVkJH5LHu");

INSERT INTO review VALUES 
(1,1,2),
(2,1,3),
(1,2,3),
(2,3,4),
(1,4,5),
(2,5,2),
(1,6,3),
(2,7,1),
(1,8,3),
(2,9,4),
(1,10,3),
(2,11,2),
(1,12,5),
(2,13,4);