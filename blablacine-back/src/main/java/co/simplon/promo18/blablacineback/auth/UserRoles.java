package co.simplon.promo18.blablacineback.auth;

public enum UserRoles {
    ROLE_USER,
    ROLE_ADMIN
}

