package co.simplon.promo18.blablacineback.entity;

import java.time.LocalDate;

public class Comment {
    private Integer id;
    private String content;
    private LocalDate date;
    private User user;
    private Movie movie;
    
    public Comment() {
    }
    public Comment(String content, LocalDate date, User user, Movie movie) {
        this.content = content;
        this.date = date;
        this.user = user;
        this.movie = movie;
    }
    public Comment(Integer id, String content, LocalDate date, User user, Movie movie) {
        this.id = id;
        this.content = content;
        this.date = date;
        this.user = user;
        this.movie = movie;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public Movie getMovie() {
        return movie;
    }
    public void setMovie(Movie movie) {
        this.movie = movie;
    }

}