package co.simplon.promo18.blablacineback.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo18.blablacineback.entity.Category;
import co.simplon.promo18.blablacineback.repository.CategoryRepository;

@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping(value="api/category")
public class CategoryController {
    @Autowired 
    CategoryRepository repo;

    @GetMapping
    public List<Category>getAll(){
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Category getOne(@PathVariable int id){
        return repo.findById(id);
    }
}
