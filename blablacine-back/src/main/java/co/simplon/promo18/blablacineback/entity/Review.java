package co.simplon.promo18.blablacineback.entity;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class Review {
    private Integer idUser;
    private Integer idMovie;
    @Min(1)
    @Max(5)
    private int score;
    
    public Review() {
    }
    
    public Review(Integer idUser, Integer idMovie, int score) {
        this.idUser = idUser;
        this.idMovie = idMovie;
        this.score = score;
    }
    public Integer getIdUser() {
        return idUser;
    }
    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }
    public Integer getIdMovie() {
        return idMovie;
    }
    public void setIdMovie(Integer idMovie) {
        this.idMovie = idMovie;
    }
    public int getScore() {
        return score;
    }
    public void setScore(int score) {
        this.score = score;
    }
}
