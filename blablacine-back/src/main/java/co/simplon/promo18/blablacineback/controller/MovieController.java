package co.simplon.promo18.blablacineback.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.blablacineback.entity.Movie;
import co.simplon.promo18.blablacineback.repository.MovieRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/movie")
public class MovieController {

    @Autowired
    MovieRepository repo;

    @GetMapping
    public List<Movie> getAll() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Movie getOne(@PathVariable int id) {
        return repo.findById(id);
    }

    @GetMapping("/category/{id}")
    public List<Movie> getByCategoryId(@PathVariable int id) {
        return repo.findByCategoryId(id);
    }

    @GetMapping("/search/{term}")
    public List<Movie> search(@PathVariable String term) {
        return repo.findByTitle(term);
    }

    @PostMapping
    public Movie post(@RequestBody Movie movie) {
        repo.save(movie);
        return movie;
    }

    @PutMapping("/{id}")
    public Movie put(@PathVariable int id, @RequestBody Movie movie) {
        if (id != movie.getId()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        repo.update(movie);
        return repo.findById(movie.getId());

}
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        repo.deleteById(id);
    }
}
