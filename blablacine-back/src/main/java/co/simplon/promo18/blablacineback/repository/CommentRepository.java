package co.simplon.promo18.blablacineback.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.blablacineback.entity.Comment;
import co.simplon.promo18.blablacineback.entity.Movie;
import co.simplon.promo18.blablacineback.entity.User;
@Repository
public class CommentRepository {
    
    @Autowired 
    MovieRepository movieRepo;
    
    @Autowired
    private DataSource dataSource;

    public List<Comment> findByMovieId(int id){
        List<Comment> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()){
            PreparedStatement stmt = connection
            .prepareStatement("SELECT * FROM comment LEFT JOIN user ON comment.id_user = user.id WHERE id_movie=? ORDER BY date DESC");
            stmt.setInt(1,id);
    ResultSet rs = stmt.executeQuery();
    while (rs.next()) {
        User user = new User(rs.getInt("user.id"), rs.getString("email"),rs.getString("user.password"),rs.getString("user.role"));
        Movie movie = new Movie();
        movie.setId(rs.getInt("id_movie"));
        Comment comment = new Comment(rs.getInt("comment.id"),rs.getString("content"),rs.getDate("date").toLocalDate(), user, movie) ;
        list.add(comment);
    }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        return list;
    }
    
    public Comment save(Comment comment){
        try (Connection connection = dataSource.getConnection()){
            PreparedStatement stmt= connection.prepareStatement("INSERT INTO comment (content,date,id_user,id_movie) VALUES (?,?,?,?)",PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1,comment.getContent());
            stmt.setDate(2,Date.valueOf(comment.getDate()));
            stmt.setInt(3,comment.getUser().getId());
            stmt.setInt(4,comment.getMovie().getId());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()){
                comment.setId(rs.getInt(1));
            }
            return comment;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }

    public void delete(int id){
       try (Connection connection = dataSource.getConnection()){
        PreparedStatement stmt = connection.prepareStatement("DELETE FROM comment WHERE id=?");
        stmt.setInt(1,id);
        stmt.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
        throw new RuntimeException("Database access error");
    }
            
        
    }
}
