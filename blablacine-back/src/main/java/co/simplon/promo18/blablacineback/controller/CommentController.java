package co.simplon.promo18.blablacineback.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo18.blablacineback.entity.Comment;
import co.simplon.promo18.blablacineback.entity.User;
import co.simplon.promo18.blablacineback.repository.CommentRepository;

@RestController
@RequestMapping(value = "api/comment")
public class CommentController {
    @Autowired
    private CommentRepository repo;

    @GetMapping("/movie/{id}")
public List<Comment> getByMovieId(@PathVariable int id){
    return repo.findByMovieId(id);
}
@PostMapping()
public Comment add(@AuthenticationPrincipal User user, @RequestBody Comment comment){
    comment.setUser(user);
    return repo.save(comment);
}
@DeleteMapping("/{id}")
public void delete(@PathVariable int id){
    repo.delete(id);
}  
}
