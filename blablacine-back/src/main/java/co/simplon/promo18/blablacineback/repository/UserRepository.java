package co.simplon.promo18.blablacineback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.blablacineback.entity.User;

@Repository
public class UserRepository {
    @Autowired DataSource dataSource;
    
    public User findById(int id){
        try (Connection connection =dataSource.getConnection()){
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE id= ?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
               User user = new User(rs.getInt("id"),rs.getString("email"),rs.getString("password"),rs.getString("role"));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        return null;
    }
    

    public User findByEmail(String term){
        try (Connection connection =dataSource.getConnection()){
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE email LIKE ?");
            stmt.setString(1,term);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
               User user = new User(rs.getInt("id"),rs.getString("email"),rs.getString("password"),rs.getString("role"));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        return null;
    }

    public User save(User user){
        try (Connection connection =dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO user(email, role, password) VALUES (?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);
          stmt.setString(1,user.getEmail());
          stmt.setString(2, user.getRole());
          stmt.setString(3, user.getPassword());
          stmt.executeUpdate();

          ResultSet rs = stmt.getGeneratedKeys();
          if(rs.next()){
            user.setId((rs.getInt(1)));
          }
          return user;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");

        }

    }
    
}
