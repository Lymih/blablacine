package co.simplon.promo18.blablacineback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlablacineBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlablacineBackApplication.class, args);
	}

}
