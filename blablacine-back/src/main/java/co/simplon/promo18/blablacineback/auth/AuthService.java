package co.simplon.promo18.blablacineback.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import co.simplon.promo18.blablacineback.entity.User;
import co.simplon.promo18.blablacineback.repository.UserRepository;

@Service
public class AuthService implements UserDetailsService{

    @Autowired
    private UserRepository repo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = new User();
        user = repo.findByEmail(username);
       if(user==null){
        throw new UsernameNotFoundException("User not found");
       }
       return user;
    }
    
}

