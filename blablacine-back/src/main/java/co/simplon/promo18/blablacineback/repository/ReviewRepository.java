package co.simplon.promo18.blablacineback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.blablacineback.entity.Review;
@Repository
public class ReviewRepository {
    @Autowired DataSource dataSource;
    
    public List<Review> findReviewByMovieId(int id) {
        List<Review> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM review WHERE id_movie=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Review review = new Review(rs.getInt("id_user"), rs.getInt("id_movie"), rs.getInt("score"));
                list.add(review);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }
    public Review findById(int idUser, int idMovie){
        try (Connection connection = dataSource.getConnection()){
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM review WHERE id_user=? AND id_movie=?");
            stmt.setInt(1, idUser);
            stmt.setInt(2, idMovie);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                Review review = new Review(rs.getInt("id_user"),rs.getInt("id_movie"),rs.getInt("score"));
                return review;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        return null;
    }
    public Review save(Review review){
        try (Connection connection = dataSource.getConnection()){
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO review VALUES (?,?,?)");
            stmt.setInt(1,review.getIdUser());
            stmt.setInt(2, review.getIdMovie());
            stmt.setInt(3, review.getScore());
            stmt.executeUpdate();
            return review;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }
    public Review update(Review review){
        try (Connection connection = dataSource.getConnection()){
            PreparedStatement stmt = connection.prepareStatement("UPDATE review SET score=? WHERE id_user=? AND id_movie=?");
            stmt.setInt(1, review.getScore());
            stmt.setInt(2, review.getIdUser());
            stmt.setInt(3, review.getIdMovie());
            stmt.executeUpdate();
            return review;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

    }
}
