package co.simplon.promo18.blablacineback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.blablacineback.entity.Category;
import co.simplon.promo18.blablacineback.entity.Movie;

@Repository
public class MovieRepository {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private ReviewRepository rRepo;

    public List<Movie> findAll() {
        List<Movie> list = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement(
                            "SELECT * FROM movie LEFT JOIN category on movie.id_category = category.id ORDER BY title");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Category category = new Category(rs.getInt("category.id"), rs.getString("label"));
                Movie movie = new Movie(rs.getInt("id"), rs.getString("title"), rs.getString("picture"),
                        rs.getString("year"), rs.getString("director"), rs.getString("description"), category);
                movie.setReviews(rRepo.findReviewByMovieId(rs.getInt("id")));
                list.add(movie);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        return list;

    }

    public Movie findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM movie LEFT JOIN category ON movie.id_category = category.id WHERE movie.id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                Category category = new Category(rs.getInt("category.id"), rs.getString("label"));
                Movie movie = new Movie(rs.getInt("id"), rs.getString("title"), rs.getString("picture"),
                        rs.getString("year"), rs.getString("director"), rs.getString("description"), category);
                movie.setReviews(rRepo.findReviewByMovieId(rs.getInt("id")));
                return movie;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        return null;
    }

    public List<Movie> findByCategoryId(int id) {
        List<Movie> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT *, category.label AS cat_label FROM movie LEFT JOIN category ON movie.id_category = category.id WHERE id_category=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            Category category = null;
            while (rs.next()) {
                if (category == null) {
                    category = new Category(rs.getInt("id_category"), rs.getString("cat_label"));
                }
                Movie movie = new Movie(rs.getInt("id"), rs.getString("title"), rs.getString("picture"),
                        rs.getString("year"), rs.getString("director"), rs.getString("description"), category);
                movie.setReviews(rRepo.findReviewByMovieId(rs.getInt("id")));
                list.add(movie);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        return list;
    }

    public List<Movie> findByTitle(String term) {
        List<Movie> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM movie LEFT JOIN category ON movie.id_category = category.id WHERE title LIKE ?");
            stmt.setString(1, "%" + term + "%");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Category category = new Category(rs.getInt("category.id"), rs.getString("label"));
                Movie movie = new Movie(rs.getInt("id"), rs.getString("title"), rs.getString("picture"),
                        rs.getString("year"), rs.getString("director"), rs.getString("description"), category);
                movie.setReviews(rRepo.findReviewByMovieId(rs.getInt("id")));
                list.add(movie);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        return list;
    }

    public Movie save(Movie movie) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO movie (title,picture,year,director,description,id_category) VALUES (?,?,?,?,?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, movie.getTitle());
            stmt.setString(2, movie.getPicture());
            stmt.setString(3, movie.getYear());
            stmt.setString(4, movie.getDirector());
            stmt.setString(5, movie.getDescription());
            stmt.setInt(6, movie.getCategory().getId());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                movie.setId(rs.getInt(1));
            }
            return movie;
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }

    public Movie update(Movie movie) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE movie SET title=?,picture=?,year=?,director=?,description=?,id_category=? WHERE id=?");
            stmt.setString(1, movie.getTitle());
            stmt.setString(2, movie.getPicture());
            stmt.setString(3, movie.getYear());
            stmt.setString(4, movie.getDirector());
            stmt.setString(5, movie.getDescription());
            stmt.setInt(6, movie.getCategory().getId());
            stmt.setInt(7, movie.getId());
            stmt.executeUpdate();
            return movie;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }

    public void deleteById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM movie WHERE id=?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }

   
}
