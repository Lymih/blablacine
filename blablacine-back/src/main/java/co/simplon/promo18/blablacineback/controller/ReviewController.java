package co.simplon.promo18.blablacineback.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.blablacineback.entity.Review;
import co.simplon.promo18.blablacineback.entity.User;
import co.simplon.promo18.blablacineback.repository.ReviewRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/review")

public class ReviewController {
    @Autowired
    ReviewRepository repo;

    @GetMapping("/{id}")
    public List<Review> getAll(@PathVariable int id) {
        return repo.findReviewByMovieId(id);
    }

    @PostMapping()
    public Review post(@AuthenticationPrincipal User user, @RequestBody Review review) {
        if (repo.findById(user.getId(), review.getIdMovie()) != null) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        } else {
            review.setIdUser(user.getId());
            repo.save(review);
            return review;
        }
    }

    @PutMapping("/{idMovie}")
    public Review put(@AuthenticationPrincipal User user, @PathVariable int idMovie, @Valid @RequestBody Review review) {
        if (user.getId() != review.getIdUser()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        if (idMovie != review.getIdMovie()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        repo.update(review);
        return repo.findById(review.getIdUser(), review.getIdMovie());
    }

    @GetMapping()
    public Review findById(int idUser, int idMovie){
        Review review=repo.findById(idUser, idMovie);
        return review;
    }
}
