package co.simplon.promo18.blablacineback.entity;

import java.util.ArrayList;
import java.util.List;

public class Movie {
    private Integer id;
    private String title;
    private String picture;
    private String year;
    private String director;
    private String description;
    private Category category;
    private List<Review> reviews = new ArrayList<>();
    
    
    public Movie() {
    }


    public Movie(String title, String picture, String year, String director, String description, Category category) {
        this.title = title;
        this.picture = picture;
        this.year = year;
        this.director = director;
        this.description = description;
        this.category = category;
    }


    public Movie(Integer id, String title, String picture, String year, String director, String description,
            Category category) {
        this.id = id;
        this.title = title;
        this.picture = picture;
        this.year = year;
        this.director = director;
        this.description = description;
        this.category = category;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Review> getReviews() {
        return reviews;
    }


    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }


}
   