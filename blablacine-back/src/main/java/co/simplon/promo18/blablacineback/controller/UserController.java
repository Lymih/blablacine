package co.simplon.promo18.blablacineback.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.blablacineback.entity.User;
import co.simplon.promo18.blablacineback.repository.UserRepository;

@CrossOrigin(origins="http://localhost:4200")
@RestController("/api/user")
@RequestMapping(value="api/user")
public class UserController {

    @Autowired 
    UserRepository repo;

    @Autowired
    private PasswordEncoder encoder;
    @PostMapping("/register")
     public User register(@Valid @RequestBody User user){
        if (repo.findByEmail(user.getEmail())!=null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }
            user.setId(null);
            user.setRole("ROLE_USER");
            String hashed = encoder.encode(user.getPassword());
            user.setPassword(hashed);
            repo.save(user);
            return user;
        }
        @GetMapping("/account")
        public User getAcount(@AuthenticationPrincipal User user){
            return user;
        }
        @GetMapping("/{email}")
        public void userExists(@PathVariable String email){
            if (repo.findByEmail(email)!=null){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
            }
        }
    };

